//originally written by Davide Byron
//this code is released under the GPLv3
//
// Reacts to E_NEW_INVENTORY_TEXTURE event
// When a new texture is received it's immediately used
// as a skin
//
// a creature with this script is able to change the texture if a user or another creature drops one
// into it's inventory. To be used it needs to have also the inventory manager script, or the
// new texture event won't be generated.
//
// WARNING: due to an unresolved bug the texture passed could not be the last one dropped

//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

//standard stuff
integer E_NEW_INVENTORY_TEXTURE = 15;
    
    
    
    
default
{
    
    on_rez(integer param)
    {
        llResetScript();
    }
    
    
    
    
    state_entry()
    {
        //this should help humans knows what this creature can do
        llSetObjectDesc( llGetObjectDesc() + "drop a texture to inventory for me to 'wear' it-" );
    }
    
    
    
    
    
    link_message( integer sender_num, integer num, string str, key id )
    {
        //if a message of a new texture arrives, put the texture on as a skin
        if(num == E_NEW_INVENTORY_TEXTURE )
        {
			//apply the texture to the whole linked set
            llSetLinkTexture(LINK_SET, str, ALL_SIDES);
        }
    }
    
}
