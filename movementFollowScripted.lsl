//originally written by Davide Byron
//this code is released under the GPLv3
//
// Reacts to E_SCRIPTED_POSITION event
// Produces R_SUB_ENERGY events
// makes the creature move in the direction of a detected active object at the cost of some energy
//
// the creature tryies to reach the object and will use an energy amount proportional to the
// distance covered with the movement.
// if there is not enough energy the creature will succed in moving but die soon after if the reached
// object does not provide food.
//
//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

//standards
integer E_SCRIPTED_POSITION = 2;
//integer E_AT_TARGET = 9;
//integer E_NOT_AT_TARGET = 10;
integer R_MOD_ENERGY = 11;
//cage boundaries expressed in region absolute coords
integer CAGE_MIN_X = 1;
integer CAGE_MAX_X = 249;
integer CAGE_MIN_Y = 1;
integer CAGE_MAX_Y = 249;
integer CAGE_MIN_Z = 1;
integer CAGE_MAX_Z = 100;




//global vars
//target we're pointing at
integer CurrentTarget;
//how many seconds to reach the point of interest
float Speed = 3.0;
//treshold for the at target event
float Proximity = 1.0;
//how far can we go in a single movement
float MaxStepLength = 40.0;
//used to calculate energy consumption, the higher the value, the less the energy required
float EnergyDividendum = 100;
//crappy stuffs
float Force = 1;
float Dump = 0.1;




//deny moving out of a definite cage
//mostly used for debug
vector checkCageLimit( vector pos )
{
    if( pos.x < CAGE_MIN_X )
        pos.x = CAGE_MIN_X;
    if( pos.x > CAGE_MAX_X )
        pos.x = CAGE_MAX_X;
    if( pos.y < CAGE_MIN_Y )
        pos.y = CAGE_MIN_Y;
    if( pos.y > CAGE_MAX_Y )
        pos.y = CAGE_MAX_Y;
    if( pos.z < CAGE_MIN_Z )
        pos.z = CAGE_MIN_Z;
    if(  llGround( pos - llGetPos() ) > pos.z )
        pos.z = llGround( pos - llGetPos() ) + 0.1;
    if( pos.z > CAGE_MAX_Z )
        pos.z = CAGE_MAX_Z;
        
    return pos;
}




//function that will calculate the movement needed to reach 
//a target in a given axis it's limited by the creature max step value
float moveTowardTarget(float creaturePos, float targetPos)
{
    //determine if we need to move forward or backward in the axis
    float direction = targetPos - creaturePos;
    if( direction >= 0 )
    {
        //if the creature can reach the object within a single "step"
        //do it
        if( targetPos - creaturePos < MaxStepLength )
            creaturePos = targetPos;
        //otherwise move as much as possible toward it
        else
            creaturePos = creaturePos + MaxStepLength;
    }
    else
    {
        //if the creature can reach the object within a single "step"
        //do it
        if( creaturePos - targetPos < MaxStepLength )
            creaturePos = targetPos;
        //otherwise move as much as possible toward it
        else
            creaturePos = creaturePos - MaxStepLength;
    }
    
    return creaturePos;
}
    
    
    
    
default
{
    
    on_rez(integer param)
    {
        llResetScript();
    }
    
    
    
    
    state_entry()
    {
        //phantom is needed to avoid collision, they cause too much lag
        llSetStatus(STATUS_PHANTOM, TRUE);
		//physical is needed to use physical moving functions
        llSetStatus(STATUS_PHYSICS, TRUE);
    }
    

    
    
    
    link_message( integer sender_num, integer num, string str, key id )
    {
        //a scripted object has been sensed
        if(num == E_SCRIPTED_POSITION )
        {
            vector newPosition = llGetPos();
            vector targetPosition = (vector)str;
            //determine the new position    
            newPosition.x = moveTowardTarget( newPosition.x, targetPosition.x );
            newPosition.y = moveTowardTarget( newPosition.y, targetPosition.y );
            newPosition.z = targetPosition.z;
            newPosition = checkCageLimit( newPosition );
            
            //point the new target...
            CurrentTarget = llTarget( newPosition, Proximity );
            //ask for energy substraction due to movement
            llMessageLinked(LINK_SET, R_MOD_ENERGY, (string)(-llVecDist(llGetPos(), newPosition)/EnergyDividendum), "");
            //look at the target
            llLookAt( newPosition, Force, Dump );
            //move
            llMoveToTarget( newPosition, Speed ); 
        }
    }
    
    
	
    
    //this has no use as of today, probably will be removed.
    at_target(integer tnum, vector targetpos, vector ourpos)
    {
        //notice the at_target event
        //llMessageLinked(LINK_SET, E_AT_TARGET, "", "");
    }
    
    
	
    
    //this has no use as of today, probably will be removed.
    not_at_target()
    {
        //notice the not_at_target event
        //llMessageLinked(LINK_SET, E_NOT_AT_TARGET, "", "");
    }
    
}
