//originally written by Davide Byron
//this code is released under the GPLv3
//
// this script senda a copy of the object it is inside (provided that
// the object as a copy of itself inside) to anyone who requests it.
// this is an experimental thing and not full developed yet. but it works.
// a full featured standard for this things is gonna be developed soon.
//
//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

//wich version of the EcoComm standard is in use, see
// http://www.slecosystem.com/wiki/index.php?title=Main_Page
//for details
// this part is not documented in the EWG wiki, as it is an improvement
// we would like to add. This MUST be considered just a preliminary version
// to test out the possibility of this approach.
// the full standard will come soon.
string ECO_COMM_VERSION = "0x1";
string TREE_NAME;
string TREE_COPY_REQUEST = "FractalTreeCopyRequest";




//credits for this function goes to Sera Rawley and her cannon plant script, thanks.
//calculates the channel used to talk to this creature, pretty much unique.
integer getEcoCommChannel(string Version, key SourceKey)
{
    return (integer)(Version + llGetSubString(SourceKey, 0, 6));
}




default
{
    
    state_entry()
    {
        TREE_NAME = llGetObjectName();
        //listen for requests
        llListen( getEcoCommChannel( ECO_COMM_VERSION, llGetKey() ), "", "", "" );
    }
    
    
    
    
    on_rez(integer param)
    {
        llResetScript();
    }




    //if a fruit requests a copy of us, give it to him
    listen( integer channel, string name, key id, string message )
    {
		//we only care that the message is the right one, no matter who is asking...
        if( message == TREE_COPY_REQUEST )
        {
            llGiveInventory(id, TREE_NAME);
        }
    }
    
}
