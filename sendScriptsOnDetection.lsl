//originally written by Davide Byron
//this code is released under the GPLv3
//
// Reacts to E_SCRIPTED_POSITION and E_ACTIVE_POSITION events
// When the creature is very near an object it has a random
// chance to send a script to it. If the other object is a creature
// with the remote loading script enabler he will receive the script
// and start using it
//
// this will allow for complex evolution to take place, as every script codes
// a specific behaviour. Other creatures will be able to acquire new behaviours
// and try to get more fit to the world.
// this is a powerful and armful tool tough, be careful as it could drive things
// out of control pretty quick.
//
//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

//standard
integer PIN = 123456;
integer E_SCRIPTED_POSITION = 2;
integer E_ACTIVE_POSITION = 4;



//global vars, may be subjected to evolution one day...
float SendingTreshold = 0.95;
float PositionTreshold = 1.0;




default
{
    //listens for messages
    link_message( integer sender_num, integer num, string str, key id )
    {
        //if a detection has occurred
        if( num == E_ACTIVE_POSITION || num == E_SCRIPTED_POSITION )
        {
            vector objPos = (vector)str;
            vector ourPos = llGetPos();
            
            //if the object is enough near
            if( llFabs(ourPos.x - objPos.x) <= PositionTreshold &&
                llFabs(ourPos.y - objPos.y) <= PositionTreshold &&
                llFabs(ourPos.z - objPos.z) <= PositionTreshold )
            {
                vector target = (vector)str;
                //the creature has a given probability of sending one of its scripts
                if( llFrand(1.0) > SendingTreshold )
                {
                    integer isRunning = TRUE;
                    llRemoteLoadScriptPin(id, llGetInventoryName(INVENTORY_SCRIPT, (integer)llFrand(
                        llGetInventoryNumber(INVENTORY_SCRIPT))), PIN, isRunning, 0 );
                }
            }
        }
    }
    
}
