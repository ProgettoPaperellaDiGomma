//originally written by Davide Byron
//this code is released under the GPLv3
//
// Makes the creature able to receive scripts from others

//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

//the PIN is part of the standard
integer PIN = 123456;




default
{
    
    on_rez(integer param)
    {
        llResetScript();
    }
    
    
    
    
    state_entry()
    {
		//see the lsl wiki for this...
        llSetRemoteScriptAccessPin( PIN );
    }
    
}
