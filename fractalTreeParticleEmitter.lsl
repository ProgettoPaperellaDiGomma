//this causes a lot of client side lag and with lots of trees the particle max is reached quickly.
//probably will be removed in a near future and it's already not being used...

//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

particles(){
     llParticleSystem(  [ 
           PSYS_SRC_TEXTURE, llGetInventoryName(INVENTORY_TEXTURE, 0), 
           PSYS_PART_START_SCALE, <.04,0.05, .04>,  
           PSYS_PART_END_SCALE, <.8,.8,.8>, 
           PSYS_PART_START_COLOR, <0,255,0>,       
           PSYS_PART_END_COLOR, <0,.3,0>, 
           PSYS_PART_START_ALPHA, 1.0,            
           PSYS_PART_END_ALPHA, 0.0,     
           PSYS_SRC_BURST_PART_COUNT, 1, 
           PSYS_SRC_BURST_RATE,  0.01,  
           PSYS_PART_MAX_AGE, 8.5, 
           PSYS_SRC_MAX_AGE, 0.0,  
           PSYS_SRC_PATTERN, 8, // 1=DROP, 2=EXPLODE, 4=ANGLE, 8=ANGLE_CONE,
           PSYS_SRC_ACCEL, <0.0,0.0,0.0>,  
        // PSYS_SRC_BURST_RADIUS, 0.0,
           PSYS_SRC_BURST_SPEED_MIN, .1,   
           PSYS_SRC_BURST_SPEED_MAX, .15, 
           PSYS_SRC_ANGLE_BEGIN,  1*DEG_TO_RAD,        
           PSYS_SRC_ANGLE_END, 0*DEG_TO_RAD,  
           PSYS_SRC_OMEGA, <0,0,0>, 
        // PSYS_SRC_TARGET_KEY,      llGetLinkKey(llGetLinkNum() + 1), 
           PSYS_PART_FLAGS, ( 0      
                                | PSYS_PART_INTERP_COLOR_MASK   
                                | PSYS_PART_INTERP_SCALE_MASK   
                                | PSYS_PART_EMISSIVE_MASK   
                                | PSYS_PART_FOLLOW_VELOCITY_MASK
                             // | PSYS_PART_WIND_MASK            
                             // | PSYS_PART_BOUNCE_MASK          
                             // | PSYS_PART_FOLLOW_SRC_MASK     
                             // | PSYS_PART_TARGET_POS_MASK     
                             // | PSYS_PART_TARGET_LINEAR_MASK    
    ) ] );
}




default
{
    
    state_entry()
    {
        particles();
    }




    on_rez(integer param)
    {
        llResetScript();
    }
    
}
