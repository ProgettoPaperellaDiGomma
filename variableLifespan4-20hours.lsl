//originally written by Davide Byron
//this code is released under the GPLv3
//
// this script just kills the tree root after a random time ranging from 3 to 7 hours.
// the root is not edible, and for this it's immortal, so another way of death is
// provided by this script. The so long distance between possible values is intended
// to make the tree population survive big lag spikes.
//
//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

//seconds, so 20 hours
float Lifespan = 72000.0;
//seconds, so 4 hours
float LifespanBias = 14400.0;



 
default
{
    
    state_entry()
    {
        //just trigger the timer
        llSetTimerEvent( Lifespan + llFrand(LifespanBias) );
    }
    
    
    
    
    on_rez(integer param)
    {
        llResetScript();
    }




    timer()
    {
        //die silently...
        llDie();
    }
    
}
