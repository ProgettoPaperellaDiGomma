//originally written by Davide Byron
//this code is released under the GPLv3
//
// Reacts to E_NEW_INVENTORY_SOUND event and has an internal timer to perform actions
// When a new sound is received it's suddenly played
// and with the timer all sounds in inventory are randomly played
//
// WARNING: due to an unresolved bug the sound passed could not be the last one dropped
//
// a creature carrying this script is able to make sounds, it randomly plays all the sounds in its inventory.
//
//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

//standard stuffs
integer E_NEW_INVENTORY_SOUND = 17;




//global vars, may be subjected to evolution one day...
//playing volume (0.0 min 1.0 max)
float SoundVolume = 1.0;
//how many sounds do we have?
integer InventorySoundsCount = 0;
//how often do we play a sound?
float NoiseFrequency = 120.0;
float NoiseFrequencyBias = 60.0;
    
    
    
  
//generates a random float number ranging from -range to range
float myRand( float range )
{
    float sign = llFrand(2.1);
    float random = llFrand( range + 0.1 );
    if( sign >= 1.0 )
        return random;
    else
        return -random;
}



  
default
{
    
    on_rez(integer param)
    {
        llResetScript();
    }
    
    
    
    
    state_entry()
    {
        //make a sound every NoiseFrequency seconds
        llSetTimerEvent( NoiseFrequency + myRand(NoiseFrequencyBias) );
        //this should help humans knows what this creature can do
        llSetObjectDesc( llGetObjectDesc() + "drop sound to inventory for me to play-" );
		InventorySoundsCount = llGeInventoryNumber( INVENTORY_SOUND );
    }
    
    
    
    
    timer()
    {
        //play a random sound among all availables
        if( InventorySoundsCount > 0 )
        {
            llPlaySound( llGetInventoryName( INVENTORY_SOUND, (integer)llFrand(InventorySoundsCount + 0.1) ),
            SoundVolume );
        }
    }
    
    
    
    
    link_message( integer sender_num, integer num, string str, key id )
    {
        //if a new sound event is catched then play immediately the new sound (gratifies the giver)
        //and increase the available sounds count
        if( num == E_NEW_INVENTORY_SOUND )
        {
            llPlaySound(str, SoundVolume);
            InventorySoundsCount++;
        }
    }
    
}
