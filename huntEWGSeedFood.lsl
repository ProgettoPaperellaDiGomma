//originally written by Davide Byron
//this code is released under the GPLv3
//
// Produces R_MOD_ENERGY, when a seed is eaten, the energy level
// is increased by notifying the energy manager script
//
// Reacts to E_SCRIPTED_POSITION and E_ACTIVE_POSITION when a target is found,
// the script checks if it's edible and in case eats it.
//
// this will only have a real effect if also the energy manager is in the creature,
// otherwise all the eating efforts will be useless.
// for eating the creature used the EWG standard, wich for now has proven good and
// well developed but might be improved in the future to assure more freedom
// from the life constrains as we know it.
//
//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

//wich version of the EcoComm standard is in use, see
// http://www.slecosystem.com/wiki/index.php?title=Main_Page
//for details
string ECO_COMM_VERSION = "0x1";
//foods i like, seeds
//this is a seed with seed food
integer PREY_MASK = 0x00100002;
//creature request for food, it eats seed
integer ECO_COMM_V1_EAT_MSG_SEED = 0x10010102;
//separator char in conformity with EcoCommV1
string ECO_COMM_V1_SEPARATOR = "|";
//reposnse from a prey, seed, it's a mask
integer ECO_COMM_V1_FOOD_MSG_SEED = 0x10010202;

//standard stuffs
integer R_MOD_ENERGY = 11;
integer E_SCRIPTED_POSITION = 2;
integer E_ACTIVE_POSITION = 4;




//how much vitality the creature gains for eating
integer EatVitalityRequested = 10;




//credits for this function goes to Sera Rawley and her cannon plant script, thanks.
//calculates the channel used to talk to this creature, pretty much unique.
integer getEcoCommChannel(string Version, key SourceKey)
{
    return (integer)(Version + llGetSubString(SourceKey, 0, 6));
}




default
{
    
    on_rez(integer param)
    {
        llResetScript();
    }
    
    
    
    
    state_entry()
    {
        //listen for eating messages
        llListen( getEcoCommChannel( ECO_COMM_VERSION, llGetKey() ), "", "", "" );
    }
    
    
    
    
    link_message( integer sender_num, integer num, string str, key id )
    {
        //a scripted object has been sensed
        if(num == E_SCRIPTED_POSITION || num == E_ACTIVE_POSITION )
        {
            //get some info
            string targetName = llGetSubString( llKey2Name(id), 0, 9 );
            
            //and check if it's edible food for us
            if( ((integer)targetName & PREY_MASK) == PREY_MASK )
            {
                //the creature reached the prey, so ask for food
                integer preyChannel = getEcoCommChannel( ECO_COMM_VERSION, id );
                llSay(preyChannel, (string)ECO_COMM_V1_EAT_MSG_SEED + ECO_COMM_V1_SEPARATOR + (string)EatVitalityRequested );
            }
        }
    }
        
     
     
        
    listen( integer channel, string name, key id, string message )
    {
        //if a prey has answered our message
        integer request = (integer)llGetSubString( message, 0, 10 );
        if( (integer)(request & ECO_COMM_V1_FOOD_MSG_SEED) != 0 )
        {
            //take vital energy from received food and signal it to the energy script
            llMessageLinked(LINK_THIS, R_MOD_ENERGY, llGetSubString(message, 11, -1 + llStringLength(message)), "");
        }
    }       
       
}
