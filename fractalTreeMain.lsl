//////////////////////////////////////////////////////////
//Base Fractal Tree Code
//Created By Ming Chen
//You can sell this code as long as you leave this line, sell it with FULL permissions, and
//modify the code just a little to make it different
//
//By the way, these are the only comments you'll see mainly through the script (because
//usually im the only one to read my code)
////////////////////////////////////////////////////////

//Modified by Davide Byron
// no comments of mine here since it's not made by me...
//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

string TREE_BRANCH_NAME;

integer totalBranches = 2;
integer minAngle = 15;
integer maxAngle = 60;
float posOffset = 1;

float MIN_POS_OFFSET = 0;
float MAX_POS_OFFSET = 2;

integer MIN_TOTAL_BRACHES = 2;
integer MAX_TOTAL_BRACHES = 4;

integer MIN_MINIMUM_ANGLE = 5;
integer MAX_MINIMUM_ANGLE = 89;

integer MIN_MAXIMUM_ANGLE = 5;
integer MAX_MAXIMUM_ANGLE = 89;

float REGEN_TIME = 540.0; //9m mins




integer checkLimits(float min, float max, float val)
{
    if(val >= min && val <= max)
        return TRUE;
    else
        return FALSE;
}




default
{
    
    state_entry()
    {
        //determine the branch name based on the inventory content
        TREE_BRANCH_NAME = llGetInventoryName(INVENTORY_OBJECT, 0);
        if( TREE_BRANCH_NAME == llGetObjectName() )
        {
            TREE_BRANCH_NAME = llGetInventoryName(INVENTORY_OBJECT, 1);
        }
        //start the timer for sprouting
        llSetTimerEvent(REGEN_TIME);
    }
    
    
    
    
    on_rez(integer param)
    {
        llResetScript();
    }




    timer()
    {
        llRezObject(TREE_BRANCH_NAME,llGetPos(),<0,0,0>,ZERO_ROTATION,1);
        llSleep(.2);
        llSay(1, llDumpList2String([totalBranches,0,minAngle,maxAngle,posOffset],"|"));  
    }




    object_rez(key id)
    {
        llGiveInventory(id,TREE_BRANCH_NAME);
    }
    
}
