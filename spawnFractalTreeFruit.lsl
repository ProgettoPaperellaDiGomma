//originally written by Davide Byron
//this code is released under the GPLv3
//
// this script takes care of spawning tree fruits at random times, untill the branch dies.
// spawning of fruits is limited by the sim lag. If there is too much lag the fruits won't be spawned
// It's also possible to spawn a fruit upon user touch on the branch.
// in this case lag is not taken into account.
//
//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

string FRUIT_NAME;




//how long between a spawning and the other
float FruitSpawningCycle = 240.0;
//bias
float FruitSpawningCycleBias = 45.0;
//max lag above which spawning is not permitted
float SpawningLagTreshold = 0.95;
//two vars to keep track of sim lag
float AverageSimLagSum = 0;
float AverageSimLagSamples = 0;




//generates a random float number ranging from -range to range
float myRand( float range )
{
    float sign = llFrand(2.1);
    float random = llFrand( range + 0.1 );
    if( sign >= 1.0 )
        return random;
    else
        return -random;
}



  
default
{
    
    on_rez(integer param)
    {
        llResetScript();
    }
    
    
    
    
    state_entry()
    {
        //delay the spawning of the fruit after the branch rezzing
        //if the branch is eaten no fruit should be spawned
        //(this should already work, but keep an eye on it...)
        FRUIT_NAME = llGetInventoryName(INVENTORY_OBJECT, 0);
        llSetTimerEvent( FruitSpawningCycle + myRand( FruitSpawningCycleBias) );
        AverageSimLagSum = llGetRegionTimeDilation();
        AverageSimLagSamples = 1;
    }




    timer()
    {
        AverageSimLagSum += llGetRegionTimeDilation();
        AverageSimLagSamples += 1;
        //the only restrain is the lag of the sim
        if( AverageSimLagSum/AverageSimLagSamples >= SpawningLagTreshold 
            && llGetRegionTimeDilation() >= SpawningLagTreshold )
        {
            //if the lag is low, let's spawn the fruit
            llRezObject(FRUIT_NAME,llGetPos(),<0,0,0>,ZERO_ROTATION,1);
        }
    }
    
    
    
    touch_start(integer touched)
    {
        //if a user touch the branch, spawn the fruit, its kinda gratifying...
        llRezObject(FRUIT_NAME,llGetPos(),<0,0,0>,ZERO_ROTATION,1);
    }
    
}
