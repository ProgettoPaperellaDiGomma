//originally written by Davide Byron
//this code is released under the GPLv3
//
// Reacts to E_NEW_INVENTORY_OBJECT event
// spawns a new child with the given object as the body
// and sends to him all his scripts (but not objects, etc...)
//
// we know nothing about the object and we don't need to know anything.
// but there is something needed anyway. the remoteloadingenabler script MUST
// be in the new body. otherwise reproduction will fail.
//
//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

//standard stuffs
integer E_NEW_INVENTORY_OBJECT = 14;
integer PIN = 123456;
//cage boundaries expressed in region absolute coords
integer CAGE_MIN_X = 1;
integer CAGE_MAX_X = 249;
integer CAGE_MIN_Y = 1;
integer CAGE_MAX_Y = 249;
integer CAGE_MIN_Z = 1;
integer CAGE_MAX_Z = 100;




//deny moving out of a definite cage
//mostly used for debug
vector checkCageLimit( vector pos )
{
    if( pos.x < CAGE_MIN_X )
        pos.x = CAGE_MIN_X;
    if( pos.x > CAGE_MAX_X )
        pos.x = CAGE_MAX_X;
    if( pos.y < CAGE_MIN_Y )
        pos.y = CAGE_MIN_Y;
    if( pos.y > CAGE_MAX_Y )
        pos.y = CAGE_MAX_Y;
    if( pos.z < CAGE_MIN_Z )
        pos.z = CAGE_MIN_Z;
    if(  llGround( pos - llGetPos() ) > pos.z )
        pos.z = llGround( pos - llGetPos() ) + 0.1;
    if( pos.z > CAGE_MAX_Z )
        pos.z = CAGE_MAX_Z;
        
    return pos;
}




//generates a random float number ranging from -range to range
float myRand( float range )
{
    float sign = llFrand(2.1);
    float random = llFrand( range + 0.1 );
    if( sign >= 1.0 )
        return random;
    else
        return -random;
}
    
    
    
    
default
{
    
    on_rez(integer param)
    {
        llResetScript();
    }
    
    
    
    
    state_entry()
    {
        //this should help humans knows what this creature can do
        llSetObjectDesc( llGetObjectDesc() + "drop item to inventory for me to reproduce-" );
    }
    
    
    
    
    
    //if this state needs to know something from the other states
    //this is the right place.
    link_message(integer sender_num, integer num, string msg, key id)
    {
        //a new object has been dropped to the creature inventory
        if( num == E_NEW_INVENTORY_OBJECT )
        {
            //so spawn a creature using that object as the body
            vector pos = llGetPos() + <myRand(2.0), myRand(2.0), myRand(2.0)>;
            pos = checkCageLimit( pos );
            llRezAtRoot(msg, pos, llGetVel(), llGetRot(), 0);
        }
    }
    
    
    
    
    //after the new creature has been spawned we send all our scripts to it
    object_rez(key id)
    {
        integer scriptCount = llGetInventoryNumber(INVENTORY_SCRIPT);
        integer i = 0;
        string script;
        //be sure scripts sent are running (if possible)
        integer isRunning = TRUE;
        
        for( ; i < scriptCount; i++ )
        {
            script = llGetInventoryName(INVENTORY_SCRIPT, i);
            //that 0 could be used in the future to cause "mutations" from one script to another...
            llRemoteLoadScriptPin(id, script, PIN, isRunning, 0 );
        }
    }
    
}
