//originally written by Davide Byron
//this code is released under the GPLv3
//
// this script checks the nearby of the newborn tree to see if there are already
// too much trees or too close trees.
// in case this one will die immediately.
//
//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

string TREE_NAME;




//how far we check for other trees
float CrowdingRange = 2.0;
//how many other trees will disturb us
integer CrowdTreshold = 2;
//not too near too
float PositionTreshold = 1.0;



default
{
    
    on_rez(integer param)
    {
        llResetScript();
    }
    
    
    
    
    state_entry()
    {
        //sense other stuffs nearby, only things with the same name
        TREE_NAME = llGetObjectName();
        llSensor( TREE_NAME, NULL_KEY, (ACTIVE|SCRIPTED), CrowdingRange, PI);
    }
    
    
    
    
    no_sensor()
    {
        //everything is fine, keep growing
    }
    
    
    
    //some others trees are in sight, let's check how many
    sensor(integer numberDetected)
    {
        if( numberDetected > CrowdTreshold )
        {
            //if there are too much we will die... sorry...
            llDie();
        }
        vector ourPos = llGetPos();
        vector nearestPos = llDetectedPos(0);
        if( llFabs( ourPos.x - nearestPos.x ) <= PositionTreshold
            && llFabs( ourPos.y - nearestPos.y ) <= PositionTreshold ) 
        {
            //if there are too near, we'll die...
            //to avoid having 10 trees on top of each other
            llDie();
        }
    }
    
}
