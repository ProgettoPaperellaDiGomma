//currently broken, will be restored asap.
//WARNING: broken, don't use it now

//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

string FRUIT_NAME = "Food (A) ";
string PREDATOR = "Creature (B) - R05.22";


default
{
    
    on_rez(integer param)
    {
        llSetObjectName( FRUIT_NAME + llGetObjectName() );
        llSetStatus(STATUS_PHYSICS, TRUE);
		llSetStatus(STATUS_PHANTOM, FALSE);
		//make it float and don't collide with the ground... maybe...
		llSetBuoyancy(1.0);
    }
    
    
    
    
    collision_start(integer numberDetected)
    {
        integer i = 0;
        
        for( ; i < numberDetected; i++ )
        {
            if( llDetectedName(i) == PREDATOR )
            {
                llDie();
            }
        }
    }
    
}
