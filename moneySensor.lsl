//originally written by Davide Byron
//this code is released under the GPLv3
//
// Produces E_MONEY_QTY_KEY event
// Money has been given to the creature, the amount is passed as a string and the giver as a key
//
// here we're messing with people money, and it's really fair if we promise them the creature will
// reproduce with the money they give to it and it doesn't what expected.
// so a creature carrying this script should be always carry an energy script too and a robust way of reproduction.
// if the Lindens are really going to put in work the patch allowing to pay 0L$ that sum will be enough
// for our needs.
//
//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

//standard stuff
integer E_MONEY_QTY_KEY = 8;
integer R_MOD_ENERGY = 11;




default
{
    
    state_entry()
    {
        //put a sign on our head to instruct avatars
        llSetText("Pay me 1L$ to help me live and reproduce", <llFrand(1.1),llFrand(1.1),llFrand(1.1)>, 1.0);
    }
    
    
    
    
    on_rez(integer param)
    {
        llResetScript();
    }
    
    
    
    
    //someone has give us money
    money(key id, integer amount)
    {
        //gratifies the user
        llSay(0, "Thank you! I'll try to reproduce thanks to your help!");
        //pass the event and the amount of money with the key of the giver
        llMessageLinked(LINK_SET, E_MONEY_QTY_KEY, (string)amount, id);
        //give energy
        llMessageLinked(LINK_SET, R_MOD_ENERGY, (string)amount, "");
    }
    
}
