//originally written by Davide Byron
//this code is released under the GPLv3
//
// Produces E_NEW_INVENTORY_OBJECT, E_NEW_INVENTORY_TEXTURE, E_NEW_INVENTORY_SCRIPT and E_NEW_INVENTORY_SOUND events
// A new object has been dropped in the inventory, the name is passed as a string
// A new texture has been dropped into the inventory, the name is passed as a string
// A new script has been dropped into the inventory, the name is passed as a string
// A new sound has been dropped into the inventory, the name is passed as a string
// WARNING!!!
// WARNING!!! this script has a substantial bug, it doesn't know the name of the last object dropped into the creature inventory.
//            this happens because SL orders the items alphabetically so the object #3 becomes #4 if another object 
//            has dropped and starts with a previous letter. I don't know a solution for this yet.
//
// except that "little" unresolved bug the script works good... it signals when something happens into the
// creature inventory, so it can react with various behaviour, most of them funny...
// this is a good script to have into your creature, it makes the creature smarter and funnier.
//
//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

//standards
integer E_NEW_INVENTORY_OBJECT = 14;
integer E_NEW_INVENTORY_TEXTURE = 15;
integer E_NEW_INVENTORY_SCRIPT = 16;
integer E_NEW_INVENTORY_SOUND = 17;




//this are needed to keep track of objects already in inventory and new ones.
integer InventoryTexturesCount = 0;
integer InventoryObjectsCount = 0;
integer InventoryScriptsCount = 0;
integer InventorySoundsCount = 0;
//lists are told to be buggy in SL... /me crosses fingers...
list InventoryStuffsList;




//this function is not a very light one, but i didn't know any other way to achieve this, so...
//it checks if an item (script, object, whatever) is already in the creature inventory
//returns 0 if the item is new, 1 otherwise and deletes the duplicate
integer checkIfItemAlreadyInInventory( integer objectType )
{
    string item2 ;
    string item;
    integer i = 0;
    integer j = -0;
    integer num = llGetListLength( InventoryStuffsList );
    integer num2 = llGetInventoryNumber( objectType );
    integer james = 0; //since it's a spy variable...
    
    //scroll all the inventory items
    for( ; j < num2; j++ ) {
        //cut the last part of the name that may contain "item 1" instead of just "item"
        item = llGetInventoryName( objectType, j);
        item2 = llGetSubString( item, 0, ( llStringLength(item) - 3 ) ); //that 3 is because the chars starts from 0
        //then compare the item with the whole list of items the creature has
        for( i = 0; i < num; i++ )
        {
            //if the item is not null and it's equal to another one in the list
            if( item2 != "" && item2 == llList2String(InventoryStuffsList, i) )
            {
                //item already present, so delete it
                llRemoveInventory( item ); //notice that we're using the full item name, not the cutted one
                james = 1;
            }
        }
    }
    
    regenInventoryList();
    
    return james;
}




//this function generates a list of all inventory items along with counter for every item category
regenInventoryList()
{
    //counters
    InventoryTexturesCount = llGetInventoryNumber(INVENTORY_TEXTURE);
    InventoryObjectsCount = llGetInventoryNumber(INVENTORY_OBJECT);
    InventoryScriptsCount = llGetInventoryNumber(INVENTORY_SCRIPT);
    InventorySoundsCount = llGetInventoryNumber(INVENTORY_SOUND);
    InventoryStuffsList = [];
        
        
    //get a list of all inventory objects
    integer i = 0;
    for( ; i < InventoryTexturesCount; i++ )
    {
        InventoryStuffsList += llGetInventoryName(INVENTORY_TEXTURE, i);
    }
    
    i = 0;
    for( ; i < InventoryObjectsCount; i++ )
    {
        InventoryStuffsList += llGetInventoryName(INVENTORY_OBJECT, i);
    }
        
    i = 0;
    for( ; i < InventoryScriptsCount; i++ )
    {
        InventoryStuffsList += llGetInventoryName(INVENTORY_SCRIPT, i);
    }
        
    i = 0;
    for( ; i < InventorySoundsCount; i++ )
    {
        InventoryStuffsList += llGetInventoryName(INVENTORY_SOUND, i);
    }
}
    
    
    
    
default
{
    
    on_rez(integer param)
    {
        llResetScript();
    }
    
    
    
    
    state_entry()
    {
        //this will let enyone drop stuffs in here
        llAllowInventoryDrop(TRUE);
    
        //make the creature aware of what's already inside itself
        regenInventoryList();
    
        //this should help humans knows what this creature can do
        llSetObjectDesc(llGetObjectDesc() + "drop items into my inventory");
    }
    
    
    
    
    //when something chages, use only data related to inventory changes
    changed(integer change)
    {
        //if the owner or someone else has dropped an item in the inventory
        if( change & CHANGED_INVENTORY || change & CHANGED_ALLOWED_DROP )
        {
            //check what kind of item was dropped and react accordingly
            integer invObjCnt = llGetInventoryNumber(INVENTORY_OBJECT);
            integer invTxtrCnt = llGetInventoryNumber(INVENTORY_TEXTURE);
            integer invScrptCnt = llGetInventoryNumber(INVENTORY_SCRIPT);
            integer invSndCnt = llGetInventoryNumber(INVENTORY_SOUND);
            
            
            //if an object has been dropped
            if( invObjCnt > InventoryObjectsCount )
            {   
                integer james = checkIfItemAlreadyInInventory( INVENTORY_OBJECT );
                
                if( james == 0 )
                {                    
                    //if an object has been dropped the creature will 
                    //accept it only if the object is full perm, this is
                    //because the creature may want to spawn a child with
                    //the new object as a body
                
                    string item = llGetInventoryName( INVENTORY_OBJECT, InventoryObjectsCount -1 );
                    //full perm mask
                    integer PERMS_OPEN = (PERM_MODIFY | PERM_COPY | PERM_TRANSFER);
                    //perms on the object
                    integer everyonePerms = llGetInventoryPermMask(item, MASK_EVERYONE);
                
                    //check
                    if (!(everyonePerms & PERMS_OPEN))
                    {
                        //since we're interactive...
                        llSay(0, "Sorry, i can accept only full perm objects, allow anyone to copy is also required ;-)");
                        llRemoveInventory(item);
                        regenInventoryList();
                    }
                    else
                    {
                        //since we're interactive...
                        llSay(0, "Thanks for this object, maybe I'll reproduce using the object you gave me as my child body\nI hope you've put the necessary scripts inside it...");
                        llMessageLinked( LINK_SET, E_NEW_INVENTORY_OBJECT, item, "" );
                    }
                }
            }
            else
            {
                //if an object has been removed, just regen the list and say ouch!
                if( invObjCnt < InventoryObjectsCount )
                {
                    regenInventoryList();
                    llSay(0, "Ouch! That hurts!!! I've lost an object");
                }
            }
            
            
            //if a texture has been received
            if( invTxtrCnt > InventoryTexturesCount )
            {   
                //check if we already have it
                integer james = checkIfItemAlreadyInInventory( INVENTORY_TEXTURE );
                
                if( james == 0 )
                {
                    //we don't have it, so it's new
                    string item = llGetInventoryName( INVENTORY_TEXTURE, InventoryTexturesCount -1 );
                    llMessageLinked( LINK_SET, E_NEW_INVENTORY_TEXTURE, item, "" );
                    //since we're interactive...
                    llSay(0, "Thanks for this new skin! Maybe I'll put it on or maybe I'll pass it to my children... Evolution is strange you know...");
                }
            }
            else
            {
                //if a texture has been deleted
                if( invTxtrCnt < InventoryTexturesCount )
                {
                    //regen the list and say ouch!
                    regenInventoryList();
                    llSay(0, "Ouch! That hurts!!! You just peeled my skin... well, almost...");
                }
            }
            
            
            //if a script has been dropped
            if( invScrptCnt > InventoryScriptsCount )
            {
                //check if we already have it
                integer james = checkIfItemAlreadyInInventory( INVENTORY_SCRIPT );
                
                if( james == 0 )
                {
                    //if we don't have it..
                    string item = llGetInventoryName( INVENTORY_SCRIPT, InventoryScriptsCount -1 );
                    llMessageLinked( LINK_SET, E_NEW_INVENTORY_SCRIPT, item, "" );
                    //since we're interactive...
                    llSay(0, "Oh my gosh! You're massively contributing to my evolution, I'm impressed!!!\nI'll try to make your script active right now, hoping it's bug free :-P");
                }
            }
            else
            {
                //if a script has been deleted
                if( invScrptCnt < InventoryScriptsCount )
                {
                    //bitch with the user
                    regenInventoryList();
                    llSay(0, "Ouch! That REALLY hurts!!! What if I cut a piece of your brain?! you're nasty!");
                }
            }
            
            
            //if a sound has been dropped
            if( invSndCnt > InventorySoundsCount )
            {
                //check if we got it already
                integer james = checkIfItemAlreadyInInventory( INVENTORY_SOUND );
                
                if( james == 0 )
                {
                    //if it's new
                    string item = llGetInventoryName( INVENTORY_SOUND, InventorySoundsCount -1);
                    llMessageLinked( LINK_SET, E_NEW_INVENTORY_SOUND, item, "" );
                    //since we're interactive...
                    llSay(0, "Unbelivable! I got a new voice now!!! I'll try to start using it ASAP, maybe also check if I have the right script for that ;-)");
                }
            }
            else
            {
                //if a sound has been deleted
                if( invSndCnt < InventorySoundsCount )
                {
                    //regen the list and say ouch
                    regenInventoryList();
                    llSay(0, "Ouch! That hurts!!! I've lost a sound");
                }
            }
        }
    }
}
