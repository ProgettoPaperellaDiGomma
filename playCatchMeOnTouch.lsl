//originally written by Davide Byron
//this code is released under the GPLv3
//
// Reacts to E_AVATAR_POSITION, E_TOUCH_ID and E_COLLISION_TYPE event
// when a touch event is catched the creature begins to play and
// activates a timer to determine the game duration
// on every avatar position event the creature will try to run away
// from her/him
// and on a collision event it will declare the avatar winner
// if the timer reaches it's event the creature win and will declare it
//
// WARNING: creature must be full physical to use this (and this script will turn it so)
//
// this gaming script is a early experiment to see how people could get involved
// in creature social life. more scripts like this will come in the future.
//
//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

//standard stuffs
integer E_AGENT_POSITION = 1;
integer E_TOUCH_ID = 7;
integer R_MOD_ENERGY = 11;
//cage boundaries expressed in region absolute coords
integer CAGE_MIN_X = 1;
integer CAGE_MAX_X = 249;
integer CAGE_MIN_Y = 1;
integer CAGE_MAX_Y = 249;
integer CAGE_MIN_Z = 1;
integer CAGE_MAX_Z = 100;





//Global vars
//how far the creature can go with a single step
float MaxStep = 20.0;
//max time to get to the target point
float Speed = 3.0;
//how long does the game last
float GameDuration = 120.0;
//crappy things
float Force = 1;
float Dump = 0.1;
//avatar wich is playing with us
key AvatarKey;
//energy gain for victory and loss
float VictoryEnergy = 100.0;
float DefeatEnergy = 40.0;



//generates a random float number ranging from -range to range
float myRand( float range )
{
    float sign = llFrand(2.1);
    float random = llFrand( range + 0.1 );
    if( sign >= 1.0 )
        return random;
    else
        return -random;
}




//deny moving out of a definite cage
//mostly used for debug
vector checkCageLimit( vector pos )
{
    if( pos.x < CAGE_MIN_X )
        pos.x = CAGE_MIN_X;
    if( pos.x > CAGE_MAX_X )
        pos.x = CAGE_MAX_X;
    if( pos.y < CAGE_MIN_Y )
        pos.y = CAGE_MIN_Y;
    if( pos.y > CAGE_MAX_Y )
        pos.y = CAGE_MAX_Y;
    if( pos.z < CAGE_MIN_Z )
        pos.z = CAGE_MIN_Z;
    if(  llGround( pos - llGetPos() ) > pos.z )
        pos.z = llGround( pos - llGetPos() ) + 0.1;
    if( pos.z > CAGE_MAX_Z )
        pos.z = CAGE_MAX_Z;
        
    return pos;
}




//function that will calculate the movement needed to escape from 
//a target in a given axis it's limited by the creature max step value
float runawayFromTarget(float creaturePos, float targetPos)
{
    //determine if we need to move forward or backward in the axis
    float direction = targetPos - creaturePos;
    if( direction >= 0 )
    {
        //flee as far as you can!!!
        creaturePos = creaturePos - MaxStep;
    }
    else
    {
        //flee as far as you can!!!
        creaturePos = creaturePos + MaxStep;
    }
    
    return creaturePos;
}
    
    
    
    
default
{
    
    on_rez(integer param)
    {
        llResetScript();
    }
    
    
    
    
    state_entry()
    {
		//collisions are required here.
        llSetStatus(STATUS_PHANTOM, FALSE);
        llSetStatus(STATUS_PHYSICS, TRUE);
		//instruct the avatars
        llSetText("Touch me and try to catch me!", <llFrand(1.1),llFrand(1.1),llFrand(1.1)>, 1.0);
        AvatarKey = "";
    }
    
    
    
    
    link_message( integer sender_num, integer num, string str, key id )
    {
        //if the avatar is in sight, try to run away
        if( num == E_AGENT_POSITION && AvatarKey != "")
        {
            vector targetPosition = (vector)str;
            vector newPosition = llGetPos();
            
            //determine the new position    
            newPosition.x = runawayFromTarget( newPosition.x, targetPosition.x );
            newPosition.y = runawayFromTarget( newPosition.y, targetPosition.y );
            newPosition.z = newPosition.z + myRand(MaxStep);
            newPosition = checkCageLimit( newPosition );
            //look at the target
            llLookAt( newPosition, Force, Dump );
            //move
            llMoveToTarget( newPosition, Speed );
        }
        
        //if the avatar touches us, the game starts if we're not already playing...
        if( num == E_TOUCH_ID && AvatarKey == "")
        {
            llShout(0, "Let's see if you can catch me human! You got " + (string)GameDuration + " seconds");
            AvatarKey = id;
			//just in case other script have overruled us...
			llSetStatus(STATUS_PHANTOM, FALSE);
			llSetStatus(STATUS_PHYSICS, TRUE);
            llSetTimerEvent(GameDuration);
        }
        
    }
    
    
    
    //in case the avatar don't touch the creature within the given time
    timer()
    {
        //the creature wins
        llShout(0, "I won, you're too slow human!");
        AvatarKey = "";
        //and get a lot of energy for this
        llMessageLinked(LINK_SET, R_MOD_ENERGY, (string)VictoryEnergy, "");
        //kill the timer too
        llSetTimerEvent(0);
    }
    
    
    
    
    //if a collision occurs
    collision_start(integer numberDetected)
    {
        //if we're paying
        if( AvatarKey != "" )
        {
            integer i = 0;
            for( ; i < numberDetected; i++)
            {
                //if one of the colliders is the avatar we're playing with
                if( llDetectedKey(i) == AvatarKey )
                {
                    //the avatar wins
                    llSay(0, "You won! Very fast human art thou");
                    AvatarKey = "";
                    //but the creature get a bit of energy anyway
                    llMessageLinked(LINK_SET, R_MOD_ENERGY, (string)DefeatEnergy, "");
                    //game over
                    llSetTimerEvent(0);
                }
            }
        }
    }
    
}
