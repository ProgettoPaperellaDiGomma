//originally written by Davide Byron
//this code is released under the GPLv3
//
// Produces E_TOUCH_ID  event
// An avatar have touched the creature, the avatar key is passed as the key
//
// the most different actions can be associated with touch, and it should
// be considered a good point for the creature, since it was able to attract
// a human with it's appearence or behaviour or whatever.
// This script notices the touch event, reactions should be specified in other scripts.
//
//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

//standard stuff
integer E_TOUCH_ID = 7;




default
{
    //a touch occurred
    touch_start(integer numberTouchers)
    {
        integer i = 0;
        //loop all touchers
        for( ; i < numberTouchers; i++)
        {
            //signal the touch event
            llMessageLinked(LINK_SET, E_TOUCH_ID, "", llDetectedKey(i));
        }
    }
}
