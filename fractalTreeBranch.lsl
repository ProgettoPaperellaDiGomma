//////////////////////////////////////////////////////////
//Tree Object Code
//Created By Ming Chen
//You can sell this code as long as you leave this line, sell it with FULL permissions, and
//modify the code just a little to make it different
//
//By the way, these are the only comments you'll see mainly through the script (because
//usually im the only one to read my code)
////////////////////////////////////////////////////////

//Slightly modified by Davide Byron
// i'm not going to comment this since i didn't understood pretty much anything :P
//
//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

string TREE_BRANCH_NAME;

integer maxCreate;
integer curLevel;
integer angleMinRotate;
integer angleMaxRotate;
float posOffset;

integer rezTotal;
integer rezzed;
integer totalGive;

integer listenKey;




integer randInt()
{
    return (integer)((llFrand(2147483647) * -1) + llFrand(2147483647));
}




default
{

    state_entry()
    {
        TREE_BRANCH_NAME = llGetObjectName();
        llAllowInventoryDrop(TRUE);
    }
    
    
    
    
    on_rez(integer param)
    {
        TREE_BRANCH_NAME = llGetObjectName();
        if(param != 0)
        {
            listenKey = llListen(param,"","","");
        }
    }




    listen(integer chan, string name, key id, string message)
    {
        llListenRemove(listenKey);
        list vals = llParseString2List(message,["|"],[]);

        maxCreate = (integer)llList2String(vals,0);
        curLevel = (integer)llList2Integer(vals,1);

        angleMinRotate = (integer)llList2Integer(vals,2);
        angleMaxRotate = (integer)llList2Integer(vals,3);

        posOffset = (float)llList2Integer(vals,4);

        rezzed = TRUE;

        vector scale = llGetScale();
        scale *= llPow(.8,curLevel);
        posOffset *= .8;
        llSetScale(scale);
        scale = llGetScale();
        llSetPos(llGetPos() + (llRot2Up(llGetRot()) * (scale.z / 2)));

    }




    changed(integer p)
    {
        if(p == CHANGED_INVENTORY && rezzed)
        {
            if(curLevel < maxCreate)
            {
                curLevel++;
                rotation rotate1 = llGetRot() * llEuler2Rot(<(llFrand(angleMaxRotate - angleMinRotate) + angleMinRotate),0,0> * DEG_TO_RAD);
                rotation rotate2 = llGetRot() * llEuler2Rot(<-(llFrand(angleMaxRotate - angleMinRotate) + angleMinRotate),0,0> * DEG_TO_RAD);
                rotation rotate3 = llGetRot() * llEuler2Rot(<0,(llFrand(angleMaxRotate - angleMinRotate) + angleMinRotate),0> * DEG_TO_RAD);
                rotation rotate4 = llGetRot() * llEuler2Rot(<0,-(llFrand(angleMaxRotate - angleMinRotate) + angleMinRotate),0> * DEG_TO_RAD);

                vector scale = llGetScale();
                vector rezPos;


                integer curChan;
                string vals = llDumpList2String([maxCreate,curLevel,angleMinRotate, angleMaxRotate,posOffset],"|");

                curChan = randInt();
                rezPos = llGetPos() + ( llRot2Up(llGetRot()) * ((scale.z / 2) - llFrand(posOffset)) );
                llRezObject(TREE_BRANCH_NAME,rezPos,<0,0,0>,rotate1,curChan);
                llSay(curChan,vals);
               

                curChan = randInt();
                rezPos = llGetPos() + ( llRot2Up(llGetRot()) * ((scale.z / 2) - llFrand(posOffset)) );
                llRezObject(TREE_BRANCH_NAME,rezPos,<0,0,0>,rotate2,curChan);
                llSay(curChan,vals);


                curChan = randInt();
                rezPos = llGetPos() + ( llRot2Up(llGetRot()) * ((scale.z / 2) - llFrand(posOffset)) );
                llRezObject(TREE_BRANCH_NAME,rezPos,<0,0,0>,rotate3,curChan);
                llSay(curChan,vals);


                curChan = randInt();
                rezPos = llGetPos() + ( llRot2Up(llGetRot()) * ((scale.z / 2) - llFrand(posOffset)) );
                llRezObject(TREE_BRANCH_NAME,rezPos,<0,0,0>,rotate4,curChan);
                llSay(curChan,vals);

            }
            else
            {
                vector scale = llGetScale();
                llShout(-1,(string)(llGetPos() + ( llRot2Up(llGetRot()) * ((scale.z / 2)))) + "|" + (string)llGetRot());
                llRemoveInventory(TREE_BRANCH_NAME);
                llSleep(1.0);
                llRemoveInventory(llGetScriptName());
            }
        }
    }




    object_rez(key id)
    {
        totalGive++;
        llGiveInventory(id,TREE_BRANCH_NAME);
        if(totalGive == 4)
        {
            llRemoveInventory(TREE_BRANCH_NAME);
            llRemoveInventory(llGetScriptName());
        }

    }
    
}