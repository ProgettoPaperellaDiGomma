//originally written by Davide Byron
//this code is released under the GPLv3
//
// Produces R_MOD_ENERGY, to inform the energy manager script of changes
// Reacts to E_SHOUT_ENERGY keeping track of the current energy amount
//
// WARNING!
// you MUST have an energy manager for this to work, otherwise you'll feed other creature forever
// and this is not a desirable behaviour.
//
// this script allow a creature to provide tree leaf food according to the EWG standard.
// TODO: it lacks a way to automagically rename the creature to make it compliant to
// the EWG naming standard in case this script is acquired during the creature life.
//
//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

//wich version of the EcoComm standard is in use, see
// http://www.slecosystem.com/wiki/index.php?title=Main_Page
//for details
string ECO_COMM_VERSION = "0x1";
//separator char in conformity with EcoCommV1
string ECO_COMM_V1_SEPARATOR = "|";
//eat request by a predator, in conformity with EcoCommV1
integer ECO_COMM_V1_EAT_MSG_TREE_LEAF = 0x1001010A;
//response to a predator request, in conformity with EcoCommV1
//notice that it already includes the separator
string ECO_COMM_V1_FOOD_MSG_TREE_LEAF = "0x1001020A|";


//standard stuffs
integer R_MOD_ENERGY = 11;
integer E_SHOUT_ENERGY = 12;


float Energy = 0;




//credits for this function goes to Sera Rawley and her cannon plant script, thanks.
//calculates the channel used to talk to this creature, pretty much unique.
integer getEcoCommChannel(string Version, key SourceKey)
{
    return (integer)(Version + llGetSubString(SourceKey, 0, 6));
}



  
default
{
    
    on_rez(integer param)
    {
        llResetScript();
    }
    
    
    
    
    state_entry()
    {
        //listen for eat messages
        llListen( getEcoCommChannel( ECO_COMM_VERSION, llGetKey() ), "", "", "" );
    }
    
    
    
    
    //credits for this function goes to Sera Rawley and her cannon plant script, thanks.
    //triggered when someone is eating this creature
    //If it's lifetime decrease too much it will die.
    listen( integer channel, string name, key id, string message )
    {
        //if a predator has sent a eat request
        if( ((integer)message & ECO_COMM_V1_EAT_MSG_TREE_LEAF) == ECO_COMM_V1_EAT_MSG_TREE_LEAF)
        {
            //find out how much food the predator needs
            integer requestedFood = (integer)llDeleteSubString(message, 0, llSubStringIndex(message, ECO_COMM_V1_SEPARATOR));
            if( Energy >= requestedFood)
            {
                //"give" him the requested food
                llSay(getEcoCommChannel(ECO_COMM_VERSION, id), ECO_COMM_V1_FOOD_MSG_TREE_LEAF + (string)requestedFood);
            }
            else
            {
                //"give" him all we have...
                llSay(getEcoCommChannel(ECO_COMM_VERSION, id), ECO_COMM_V1_FOOD_MSG_TREE_LEAF + (string)Energy);
            }   
            //signal the loss of energy to the creature
            llMessageLinked(LINK_THIS, R_MOD_ENERGY, (string)(-requestedFood), "");
            
        }
    }
    
    
    
    
    link_message( integer sender_num, integer num, string str, key id )
    {
        //keep our energy value in line with the creature energy level, periodically updated
        if( num == E_SHOUT_ENERGY )
        {
            Energy = (float)str;
        }
    }
    
}
