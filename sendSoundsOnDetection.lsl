//originally written by Davide Byron
//this code is released under the GPLv3
//
// Reacts to E_SCRIPTED_POSITION and E_ACTIVE_POSITION events
// When the creature is very near an object it has a random
// chance to send a sound to it.
//
// you can see this as a way of learning to talk for creatures
// a real comunication standard will be defined at some point in
// the future and we can bound that to sounds maybe...
//
//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

//standard
integer E_SCRIPTED_POSITION = 2;
integer E_ACTIVE_POSITION = 4;



//global var, cna be subject of evolution some day..
float SendingTreshold = 0.95;
float PositionTreshold = 1.0;



default
{
    link_message( integer sender_num, integer num, string str, key id )
    {
        //if a detection has occurred
        if( num == E_ACTIVE_POSITION || num == E_SCRIPTED_POSITION )
        {
            vector objPos = (vector)str;
            vector ourPos = llGetPos();
            
            //if the object is enough near
            if( llFabs(ourPos.x - objPos.x) <= PositionTreshold &&
                llFabs(ourPos.y - objPos.y) <= PositionTreshold &&
                llFabs(ourPos.z - objPos.z) <= PositionTreshold )
            {
                vector target = (vector)str;
                //we have a chance to send a sound
                if( llFrand(1.0) > SendingTreshold )
                {
                    if( llGetInventoryNumber(INVENTORY_SOUND) > 0 )
                    {
                        //a random one among all we have
                        llGiveInventory(id, llGetInventoryName(INVENTORY_SOUND, (integer)llFrand(
                                        llGetInventoryNumber(INVENTORY_SOUND))) );
                    }
                }
            }
        }
    }
    
}
