//originally written by Davide Byron
//this code is released under the GPLv3
//
// Reacts to E_NEW_INVENTORY_TEXTURE event
// When a new texture is received it's immediately used
// as a shape
//
// when a creature has this script it gains the ability to change the shape by apllying a sculpted texture.
// this is useful to have complex shaped creatures that uses just one prim, but it has the shortcoming of
// creatures with multi prims that can acquire this script via evolution. In that case results are unpredictable
// and untested. Be careful and please report back any discovery :)
//
// WARNING!!! this causes your creature to have very weird shapes if the texture passed is not a sculpted.
//            until a standard for naming texture or something is not in place it should be avoided

//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

//standard related
integer E_NEW_INVENTORY_TEXTURE = 15;



 
default
{
    
    on_rez(integer param)
    {
        llResetScript();
    }
    
    
    
    
    state_entry()
    {
        //this should help humans knows what this creature can do
        llSetObjectDesc( llGetObjectDesc() + "drop sculpted textures into inventory-");
		//no need to init the object as sculpted right now, the first texture received will turn it
		//into a sculpted and apply immediately the texture received.
    }
    
    
    
    
    
    link_message( integer sender_num, integer num, string str, key id )
    {
        //if a message of a new texture arrives, put the texture on as a skin
        if(num == E_NEW_INVENTORY_TEXTURE )
        {
            //turn the object into a sculpted and apply the sculpted texture
            llSetPrimitiveParams([PRIM_TYPE, PRIM_TYPE_SCULPT, str, PRIM_SCULPT_TYPE_SPHERE]);
        }
    }
    
}
