//originally written by Davide Byron
//this code is released under the GPLv3
//
// Produces E_PASSIVE_POSITION and E_NO_SENSOR events
// Sensor have detected a passive object, the position is passed as a string
// Sensor have not detected anything, no params
//
// this sensor is a kind of eye for the creature. Almost everything about the external world
// is known via this script. Succesfully detecting food is a key feature for surviving.
//
//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

//standard constants
integer E_PASSIVE_POSITION = 3;
integer E_NO_SENSOR = 5;




//vars used by the script, can be subjected to evolution one day...
float SensorRange = 96.0;
float SensorFrequency = 20.0;
    
    
    
    
default
{
    
    on_rez(integer param)
    {
        llResetScript();
    }
    
    
    
    
    state_entry()
    {
        //Activate the sensor
        llSensorRepeat("", "", PASSIVE, SensorRange, PI, SensorFrequency);
    }
    



    //nothing in sight
    no_sensor()
    {
        llMessageLinked(LINK_SET, E_NO_SENSOR, "", "");
    }

    
        
    //something in sight, investigate further            
    sensor(integer numberDetected)
    {
        //pick a random object detected and broadcast it's coords within the animal body
        integer target = (integer)llFrand( numberDetected );
        llMessageLinked(LINK_SET, E_PASSIVE_POSITION, (string)llDetectedPos(target), llDetectedKey(target));
    }
    
}
