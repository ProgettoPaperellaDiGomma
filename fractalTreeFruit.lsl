//originally written by Davide Byron
//this code is released under the GPLv3
//
// this script is a tree fruit, it starts on a branch, after a while it ripens
// and falls to the ground. 
// When the fruit is still attached to the tree it should be able
// to get a copy of the tree root (and maybe even some other things,
// like an animal for example), in that case it has a chance to reproduce
// when it ripens and falls to the ground.
// This script has 2 states, one for the fruit on the tree, and one for the ripen time.
//
//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

//wich version of the EcoComm standard is in use, see
// http://www.slecosystem.com/wiki/index.php?title=Main_Page
//for details
// this part is not documented in the EWG wiki, as it is an improvement
// we would like to add. This MUST be considered just a preliminary version
// to test out the possibility of this approach.
// the full standard will come soon.
string ECO_COMM_VERSION = "0x1";
string TREE_COPY_REQUEST = "FractalTreeCopyRequest";
string FRUIT_NAME;




//time to ripen (how the fruit hangs on the tree)
float FruitRipen = 40.0;
//bias to ripen
float FruitRipenBias = 15.0;
//time to spawn a plant (how the fruit spends on ground before spawning a plant)
float PlantDevelopment = 60.0;
//spawning bias
float PlantDevelopmentBias = 40.0;
//max chance of spawning a tree
float SpawningChance = 1.1;
//treshold to be reached for spawning
float SpawningTreshold = 0.5;




//generates a random float number ranging from -range to range
float myRand( float range )
{
    float sign = llFrand(2.1);
    float random = llFrand( range + 0.1 );
    if( sign >= 1.0 )
        return random;
    else
        return -random;
}




//credits for this function goes to Sera Rawley and her cannon plant script, thanks.
//calculates the channel used to talk to this creature, pretty much unique.
integer getEcoCommChannel(string Version, key SourceKey)
{
    return (integer)(Version + llGetSubString(SourceKey, 0, 6));
}




default
{
    
    state_entry()
    {
        FRUIT_NAME = llGetObjectName();
        llSetStatus( STATUS_PHANTOM, TRUE);
        llSetStatus( STATUS_PHYSICS, FALSE);
        //stay on the tree till ripen
        llSetTimerEvent( FruitRipen + myRand( FruitRipenBias) );
        llAllowInventoryDrop( TRUE );
        //meanwhile ask for a copy of the tree
        llSensor( "", NULL_KEY, (ACTIVE|SCRIPTED), 20.0, PI); //20.0 is the range of llSay
    }
    
    
    
    
    on_rez(integer param)
    {
        llResetScript();
    }




    //no trees in range, bad thing...
    no_sensor()
    {
        //the fruit cannot develop a plant, so it must die
        //but it's up to the energy manager to kill it
		//as it could be food meanwhile.
    }
    
    
    
    
    //tree is maybe in range, it will give us the needed copy
    sensor(integer numberDetected)
    {
        //we gotta ask to everyone tough...
        integer i = 0;
        for( ; i < numberDetected; i++)
        {
            llSay( getEcoCommChannel(ECO_COMM_VERSION, llDetectedKey(i)), TREE_COPY_REQUEST );
        }
    }
    
    
    
    
    timer()
    {
        //when ripen, turn it physical so it will fall
        //but keep the phantom too, to avoid lag
        llSetStatus( STATUS_PHYSICS, TRUE );
        //go to the next state to develop a plant
        state develop;
    }
    
}




state develop
{
    
    state_entry()
    {
        //wait some time before spawning a new plant, and hope to not be eaten
        llSetTimerEvent( PlantDevelopment + myRand(PlantDevelopmentBias) );
    }
    
    
    
    
    timer()
    {
		//this will develop a plant only if the object is in inventory
		//and we score a certain random value (so not all fruits will spawn)
        if( llGetInventoryNumber(INVENTORY_OBJECT) > 0 && llFrand(SpawningChance) > SpawningTreshold )
        {
            //if you're here and you got the tree you won! a new plant will develop from you! congrats
            llRezObject(llGetInventoryName(INVENTORY_OBJECT, 0),llGetPos(),<0,0,0>,ZERO_ROTATION,1);
        }
    }
    
    
    
    
    //after rezzing the new tree, we MUST give it a copy of itself for life to go on...
    object_rez(key id)
    {
        llGiveInventory(id, llGetInventoryName(INVENTORY_OBJECT, 0));
        //naturally you die... sad fate...
        llDie();
    }
    
}