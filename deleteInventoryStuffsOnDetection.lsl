//originally written by Davide Byron
//this code is released under the GPLv3
//
// Reacts to E_SCRIPTED_POSITION and E_ACTIVE_POSITION events
// When the creature is very near with an object it has a random
// chance to delete some inventory stuff (objects, scripts, sounds).
// --- notice, this script is very dangerous for creatures, is more like a virus ---
//
// creatures carrying this script have a kind of illnes and they're behaviour will change over time as they
// loose more and more scripts. This will eventually bring to death in most cases, but it could happen that
// the bad script deletes itself, leaving the creature alive and able to survive, but with a different behaviour.
// use with care.
//
//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

//standard
integer E_SCRIPTED_POSITION = 2;
integer E_ACTIVE_POSITION = 4;




//global var, may evolve in the future
//chance to loose an item
float DeletingTreshold = 0.98;
//distance from the other object/creature
float PositionTreshold = 1.0;
//don't delete things if the total is less than this
integer MinInventoryItems = 0;




default
{
    
    link_message( integer sender_num, integer num, string str, key id )
    {
        //a detection has occurred
        if( num == E_ACTIVE_POSITION || num == E_SCRIPTED_POSITION )
        {
            vector objPos = (vector)str;
            vector ourPos = llGetPos();
            
            //if the object is close enough
            if( llFabs(ourPos.x - objPos.x) <= PositionTreshold &&
                llFabs(ourPos.y - objPos.y) <= PositionTreshold &&
                llFabs(ourPos.z - objPos.z) <= PositionTreshold )
            {
                //so with a high chance an item in the inventory will be lost
                if( llFrand(1.0) > DeletingTreshold )
                {
                    integer inventoryNumberAll = llGetInventoryNumber(INVENTORY_ALL);
                    //don't let the creature remain without anything at all
                    if( inventoryNumberAll > MinInventoryItems )
                    {
						//remove something at random
                        llRemoveInventory( llGetInventoryName(INVENTORY_ALL,
                             (integer)llFrand(inventoryNumberAll)) );
                    }
                }
            }
        }
    }
    
}
