//originally written by Davide Byron
//this code is released under the GPLv3
//
// Produces E_SHOUT_ENERGY, the energy level is shouted within the creature 
// at regular intervals, it's a float passed as a string
//
// Reacts to R_MOD_ENERGY adding the new value to the current energy. If a substraction
// is needed the value should be passed as negative as this script always adds.
//
// NOTICE
// all other script must be aware of energy within the creature for this one to work properly
//
// this is the base fitness function for now and can gain energy from a vast variety of events
// along with this script the creature should have at least one source of energy, otherwise it will die
// pretty soon. Energy can be gained from other creatures, avatars, avatars actions (touch, money) etc...
// other energy sources will come in the future.
// the shouting of energy is necessary to inform other energy aware scripts of the current energy level
// in the creature body. this can (and often does) lead to small incoherences between energy values in
// different scripts. There is no fix for that atm but it's not a big trouble. any contribution is welcome.
//
//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

//standard stuff
integer R_MOD_ENERGY = 11;
integer E_SHOUT_ENERGY = 12;




//how much energy the creature starts with
float INITIAL_ENERGY = 400.0;




//how often the creature looses energy
float AgingRate = 60.0;
//the current amount of energy
float Energy;




//FIXME: this function need further customization for the future...
die()
{
    //comment me out if you don't want to be bothered....
    llOwnerSay("I'm dying because i've run out of energy... see you!");
    llDie();
}




default
{
    
    state_entry()
    {
        Energy = INITIAL_ENERGY;
        llSetTimerEvent(AgingRate);
    }
    
    
    
    
    on_rez(integer param)
    {
        llResetScript();
    }
    
    
    
    
	//this is mainly for debug, probably will be deleted in the future.
    touch_start(integer touched)
    {
        llSay(0, "Energy left = " + (string)Energy);
    }
    
    
    
    
    //every timer tick the creature loses energy, just for being alive...
    timer()
    {
        //energy loss is directly proportional to sim lag
        Energy -= (1 - llGetRegionTimeDilation());
        if( Energy <= 0 )
        {
            die();
        }
        //shouts the energy level for others to ear
        llMessageLinked(LINK_SET, E_SHOUT_ENERGY, (string)Energy, "");
    }
    
    
    
    
    //adds or substracts energy on request.
    //the operation here is always an addition, it's up to requesting script
    //to turn the number negative in case of energy loss.
    link_message( integer sender_num, integer num, string str, key id )
    {
        if( num == R_MOD_ENERGY )
        {
            //this is actually a substraction if the value of str is negative
            Energy += (float)str;
        }
    }
    
}
