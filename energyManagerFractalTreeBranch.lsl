//originally written by Davide Byron
//this code is released under the GPLv3
//
// Produces E_SHOUT_ENERGY, the energy level is shouted within the creature 
// at regular intervals, it's a float passed as a string
//
// Reacts to R_MOD_ENERGY adding the new value to the current energy. If a substraction
// is needed the value should be passed as negative as this script always adds.
//
// NOTICE
// all other script must be aware of energy within the creature for this one to work properly
//
// WARNING
// Slightly modfied version to fit the Tree
//
// this is the base fitness function for now and can gain energy from a vast variety of events
// along with this script the creature should have at least one source of energy, otherwise it will die
// pretty soon. Energy can be gained from other creatures, avatars, avatars actions (touch, money) etc...
// other energy sources will come in the future.
// the shouting of energy is necessary to inform other energy aware scripts of the current energy level
// in the creature body. this can (and often does) lead to small incoherences between energy values in
// different scripts. There is no fix for that atm but it's not a big trouble. any contribution is welcome.
//
//dedicated to Mealea, thanks for the passion you put into things and for being able to pass it on to me :)

integer R_MOD_ENERGY = 11;
integer E_SHOUT_ENERGY = 12;




float INITIAL_ENERGY = 9.0;




//how fast the energy is lost
float AgingRate = 5.0;
//a little bias to improve avoiding lag
float AgingRateBias = 1.5;
//current energy level
float Energy;




//generates a random float number ranging from -range to range
float myRand( float range )
{
    float sign = llFrand(2.1);
    float random = llFrand( range + 0.1 );
    if( sign >= 1.0 )
        return random;
    else
        return -random;
}




die()
{
    llDie();
}




default
{
    
    state_entry()
    {
        //initialize aging process
        Energy = INITIAL_ENERGY;
        llSetTimerEvent(AgingRate + myRand(AgingRateBias));
    }
    
    
    
    
    on_rez(integer param)
    {
        llResetScript();
    }
   
   
   
    
    //every timer tick the creature loses energy, just for being alive...
    timer()
    {
        Energy -= (1 - llGetRegionTimeDilation() );
        if( Energy <= 0 )
        {
            die();
        }
        //shouts the energy level for others to ear
        llMessageLinked(LINK_THIS, E_SHOUT_ENERGY, (string)Energy, "");
    }
    
    
    
    
    //adds or substracts energy on request.
    //the operation here is always an addition, it's up to requestion script
    //to turn the number negative in case of energy loss.
    link_message( integer sender_num, integer num, string str, key id )
    {
        if( num == R_MOD_ENERGY )
        {
            //this is actually a substraction if the value of str is negative
            Energy += (integer)str; 
        }
    }
    
}
